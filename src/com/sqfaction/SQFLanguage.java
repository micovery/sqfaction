package com.sqfaction;

import com.intellij.lang.Language;

public class SQFLanguage extends Language {
  public static final SQFLanguage INSTANCE = new SQFLanguage();

  private SQFLanguage() {
    super("SQF");
  }
}