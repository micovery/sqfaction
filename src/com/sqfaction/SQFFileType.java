package com.sqfaction;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class SQFFileType extends LanguageFileType {
  public static final SQFFileType INSTANCE = new SQFFileType();

  private SQFFileType() {
    super(SQFLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "SQF";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "Status Quo Function";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "sqf";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return SQFIcons.FILE;
  }
}