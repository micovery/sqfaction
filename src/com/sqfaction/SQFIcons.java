package com.sqfaction;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class SQFIcons {
  public static final Icon FILE = IconLoader.getIcon("/com/sqfaction/icons/sqfIcon.png");
}