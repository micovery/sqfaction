package com.sqfaction;


import com.intellij.openapi.fileTypes.*;
import org.jetbrains.annotations.NotNull;

public class SQFFileTypeFactory extends FileTypeFactory {
  @Override
  public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
    fileTypeConsumer.consume(SQFFileType.INSTANCE, "sqf");
  }
}